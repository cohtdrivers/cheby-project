\documentclass{article}
\usepackage{palatino}
\usepackage{graphicx}
\usepackage{array}
\usepackage{color,colortbl}

\title{Hardware design tooling:\\
	a project proposal}
\author{Tristan Gingold\\
	Juan David Gonz\'alez Cobas}
\date{November 17, 2017}

\begin{document}
\maketitle

\section{Background}

Hardware design teams in different groups at CERN (esp. in the AT
sector) have historically developed tools and workflows that share many
common features.

A shared pattern among most of these tools is the reliance on a
description of the hardware at a suitable abstraction level (e.g. a
single XML file edited with Cheburashka, CSV files, a CCDB description,
or a Lua table). From this single data source, code is generated for
components like HDL IP cores,  device drivers
and even FESA classes.

Unfortunately, the historical lack of a global approach has led to a
certain duplication of effort, lack of maintenance or synchronization
with changing CO frameworks,  and the ensuing obsolescence of certain
tools or parts thereof.

It is our purpose here to propose a way to arrive at a coherent, unified
set of tools that

\begin{enumerate}
\item covers the entire feature set of current tools like wbgen,
Cheburashka, encore and HDLmake,
\item fills current gaps in existing tools (e.g. produce the
toplevels/interconnects currently absent from wbgen)
\item can be maintained in the future, keeping up with changes in
CO infrastructure (e.g. FESA, drivers, etc.)
\item furnishes a direct device test environment for low-level debugging
purposes (what some people call ``a new \texttt{nodal}'').
\end{enumerate}

\section{Elements of the system}

The common needs that the project will have to cover are the following.

\subsection{A common hardware description format}
\label{format}
Different tools use different formats: Cheburashka has its own,
especially complete; encore picks descriptions in a flat form from CCDB
or CSV files, while wbgen takes a Lua table.

Ideally, completing the Cheburaska format to cover the features it is
missing from wbgen would be satisfactory. Dropping XML in favour of
a human-readable format is an additional possibility (the underlying
model, however, should remain).

This way, all tools can interoperate, and share the same data structure,
to represent hardware consistently.

\emph{The important point in this area is to share a common data model
(i.e. the semantics of the description format), not the format as an end
in itself.}

We don't plan to converge with SILECS in this project. SILECS also
describes data structures, but using higher level types (like floats)
and without low-level representation and bit meaning.

\subsection{A graphical editor to integrate the workflow}

This is already done by Cheburashka, although the integration of the
different phases is not homogeneous (e.g. Gena is called as an external
tool, encore is invoked through a CSV converter, etc.)

Integration of the entire workflow in the graphical tool is a goal,
which should not preclude command-line based workflow.

\subsection{An HDL code generator}

Currently, Gena fits the bill nicely. A nice goal here would be to
integrate the features missing from wbgen (e.g. Wishbone compatibility).

\subsection{A firmware build system}

The offer here is quite heterogeneous. BE-CO relies on Hdlmake, RF has
its own system based on Python scripts, and BE-BI uses Altera, which is
not covered by the above.

\subsection{A device driver generator}

Today, encore does this jobs nicely, although it needs a good cleanup
and lots of new features require to be implemented (in particular, PCI
driver generation, C++ wrappers, bitfields, among many others). Although
bridging through CSV is a possibility to keep, integrating the common
data structure for hardware description would be ideal in this context.

\subsection{A FESA class generator}

At least a working (=compiling) FESA template should be generated for
the particular hardware type involved. This is the original Cheburashka
\emph{raison d'\^etre}, and is also in the wish list from BE/BI. Here,
ideally, BE/CO should take a part, if such a generator has to be
maintained in the long term, and realistically keeping up with changes
in the FESA framework is a goal.

\subsection{A documentation generator}

Both wbgen and Cheburashka (TBC) are able to generate an HTML (or
latex or texinfo) file to document the register file.  The wbgen tool
also produce a drawing representing the design (as a box) and the
ports.

\section{Architecture}

We propose here a modular architecture that would make the system much
more coherent and future-proof that all the current isolated solutions.
See Fig.~\ref{architecture}. All the products for the target hardware
type (HDL code, device driver or library, FESA class) will be generated
from a single unified description in the universal format referred to
in~\ref{format}.

The three main code generators (HDL, drivers, FESA) must
\begin{itemize}
\item be standalone, i.e., it must be possible to use any one tool
independently of the others (e.g. to produce only a driver and nothing
else)
\item agnostic about any graphical editor or possible external workflow
\item only commonality is the same model of hardware descrption (hardware
description format or its underlying data model).
\item offer a way to integrate in the graphical editor as a plugin
without degrading command-line based workflow.
\end{itemize}

\begin{figure}
\includegraphics[bb=0 0 763 442,width=0.9\textwidth]{architecture.pdf}
\caption{Overview of tools involved in code generation for the
	control system}
\label{architecture}
\end{figure}

This way, the following scenario would be possible: FESA code generation
is developed and maintained by FESA experts (say in BE-CO), and kept in
sync with FESA framework evolution. The current encore driver generation
is kept, but integrated in the graphical tool suppressing the need for
CSV conversions. HDL production can be integrated in the graphical tool
as a Gena/wbgen plugin, or a merge thereof that adds to Gena the
features from wbgen that it currently lacks. Note that different teams
work here in their respective domains avoiding duplication of effort and
long-term maintainability problems.

This adds much more flexibility as well, given that no tool is highly
coupled with any other, which makes the whole design more resilient to
failure (like teams dropping out).

To summarize, the system will use a common model and a core, plug-in
based tool that handles the model with loose coupling with the different
code-generating plugins.

\section{Work packages}

This is the current vision of BE-CO-HT of the work to be done.  It has to
be discussed with other parties.

\subsection{WP1: Defining a common description format}

Parties must agree on a common file format.  Possible options are:
\begin{description}
\item [XML] This is the current input format used by Cheburashka. It may not
be easily readable by humans (although we think it is currently), but it is
easily handled by tools and has a description of the syntax (the schema).
\item [JSON] More compact and easily readable than XML, but lacks some
interesting features like comments
\item [YAML] More compact than XML and more powerful than JSON
\end{description}

The file format must be documented. There should be a process to update the
format so that each party can adapt the format to its need without creating
non-public extensions.

\begin{description}
\item [Participants:] all the parties.
\item [Leader:] BE-CO (Tristan)
\end{description}

\subsection{WP2: Migration}

Most or even all parties may have to write a converter from their current
format to the new format.

\begin{description}
\item [Participants:] each party may write a converter from their legacy file
format.
\end{description}

\subsection{WP3: Reader/Parser}

That would be an evolution of PyCheb: write a library that reads the description
and create an internal representation of it (in Python).

\begin{description}
\item [Participants:] TBD.
\end{description}

\subsection{WP4: Graphical editor}

Evolution of Cheburashka (an Eclipse plugin). The ability to base the
workflow on command-line based tools should be kept.

\begin{description}
\item [Participants:] BE-OP
\item [Leader:] Anthony
\end{description}

\subsection{WP5: HDL code generator}

Evolution of Gena and wbgen, and eventual merge thereof.

Code generated should not used proprietary blocks; ideally generated
code should be standalone.

\begin{description}
\item [Participants:] BE-BI and BE-CO
\end{description}

\subsection{WP6: C header generator}

This goes from a simple C header file that represents the memory map to
an input file for encore.

\begin{description}
\item [Participants:] BE-CO
\end{description}

\subsection{WP7: FESA class generator}

BE/CO/SRC has agreed to provide a stable FESA library. The initial generator
will be extracted from Cheburashka and rewritten in Python.  There are three different parts:
\begin{itemize}
\item Encore/C++ wrapper generation (using either raw values or converted values)
\item FESA code generation (Set/Get/RTaction)
\item FESA XML generation
\end{itemize}

\begin{description}
\item [Participants:] BE-RF, hand over to BE-CO.
\end{description}

\subsection{WP8: Documentation generation}

The content and the format of the documentation is not yet defined, the tool
should be modular enough so that various outputs are possible.

\begin{description}
\item [Participants:] TBD.
\end{description}

\subsection{WP9: CCDB Importer}

The register map is described in the CCDB, Encore is able to use the
CCDB as a source to generate a driver.  But currently this is no
automatic way to fill the CCDB from the description file.  This is a
missing piece, but I am not sure any party is interested by such a
tool.

\begin{description}
\item [Participants:] BE-CO
\item [Leader:] David
\end{description}

\subsection{WP10: HDLMake integration}

Currently in the BE-CO-HT workflow, documentation and vhdl files are
generated manually (or with a manually written script).  As a consequence,
there is neither a consistency check nor a global view.  Software developpers
complain there is no a global register map for the device.

If the tools were integrated with HDLMake, it would be possible to create a
global map and documentation.

\begin{description}
\item [Participants:] BE-CO
\end{description}

\subsection{WP11: Direct hardware test interface}

Currently PyUAL provides a GUI to read and write registers.  However, it
uses its own file format (YAML based) for the list of registers.  This
effort is the right place to improve this tool and use the common file
format. CLI-based hardware access (\'a la \texttt{nodal}) should be kept
as well.  Graphical remote access should be added.

\begin{description}
\item [Participants:] BE-CO
\end{description}

\begin{table}[t]
\centering
\begin{tabular}{p{0.4\textwidth}p{0.4\textwidth}r}
\hline\noalign{\smallskip}
Component			&Developers						&Responsibility \\
\hline\noalign{\smallskip}
Graphical editor		&Anthony Rey				 (BE-OP)	&BE-OP	\\
Hardware description schema	&Tristan Gingold			 (BE-CO)	&BE-CO	\\
Readers/parsers			&Tristan Gingold			 (BE-CO)	&BE-CO	\\
Migration from wbgen		&Tristan Gingold			 (BE-CO)	&BE-CO	\\
FESA code generation		&Bartosz Bielawski and\newline
						Przemyslaw Plutecki
									 (BE-RF)	&BE-RF	\\
encore connection		&Michel Arruat and\newline David Cobas	 (BE-CO)	&BE-CO	\\
encore C++ wrapper		&Michel Arruat and\newline David Cobas	 (BE-CO)	&BE-CO	\\
encore evolution		&Michel Arruat and\newline David Cobas	 (BE-CO)	&BE-CO	\\
CCDB integration 		&David Cobas and \newline
						\L ukasz Burdzanowski	 (BE-CO)	&BE-CO	\\
PyUAL				&Federico Vaga				 (BE-CO)	&BE-CO	\\
HDL code generation		&Tom Levens				 (BE-BI)	&BE-CO	\\
HDL interconnect generation	&Tristan Gingold			 (BE-CO)	&BE-CO	\\
wbgen port/extension		&Tristan Gingold			 (BE-CO)	&BE-CO	\\
\hline
\end{tabular}
\label{partage}
\caption{Work packages, developers and eventual responsibilities}
\end{table}

\newcommand{\rdvmark}{$\Rightarrow$}
\definecolor{Gray}{gray}{0.9}
\begin{table}[b]
\centering
\begin{tabular}{lrrrp{0.4\textwidth}}
\hline\noalign{\smallskip}
WP	&begin (wk)	&end (wk) &end (date)&	Milestone \\
\hline\noalign{\smallskip}
WP1&	1&	9&	end Feb 2018&			When the core description format is defined	\\
\rowcolor{Gray}
\multicolumn{1}{c}{\rdvmark}&		9  &	\multicolumn{2}{l}{ }&{rdv file definition (all)} \\
WP3&	9&	13&	end Mar 2018&			Reader/Parser	\\
\rowcolor{Gray}
\multicolumn{1}{c}{\rdvmark}& 9  &		\multicolumn{2}{l}{ }&{rdv CCDB schema/functions (Lukasz)} \\
WP9&	9&	26&	end Jun 2018&			CCDB Importer	\\
WP7&	13&	26&	end Jun 2018&			FESA class generator (I)	\\
\rowcolor{Gray}
\multicolumn{1}{c}{\rdvmark}&		26 &	\multicolumn{2}{l}{ }&{rdv FESA core API (Fred)} \\
WP7&	26&	44&	end Oct 2018&			FESA class generator (II)	\\
WP4&	13&	44&	end Oct 2018&			Graphical editor	\\
WP5&	13&	26&	end Jun 2018&			HDL code generator (I)	\\
\rowcolor{Gray}
\multicolumn{1}{c}{\rdvmark}&		26 &	\multicolumn{2}{l}{ }&{rdv wbgen/Gena users} \\
WP5&	26&	35&	end Aug 2018&			HDL code generator (II)	\\
WP6&	13&	20&	mid May 2018&			driver code generation (.c, .h, c++ wrapper) (I)	\\
\rowcolor{Gray}
\multicolumn{1}{c}{\rdvmark}&		20 &	\multicolumn{2}{l}{ }&{rdv EG encore (BI/RF/MPE/ABT)} \\
WP6&	20 &	26&	end Jun 2018&			driver code generation (.c, .h, c++ wrapper) (II)	\\
WP11&	13&	44&	end Oct 2018&			Graphical hardware interface	\\
WP2&	35&	44&	end Oct 2018&			Converters	\\
WP8&	35&	44 &	end Oct 2018&			Documentation generation	\\
WP10&	35&	44&	end Oct 2018&			HDLMake integration	\\
\hline
\end{tabular}
\label{timeline}
\caption{Project timeline}
\end{table}

\end{document}

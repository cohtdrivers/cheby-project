.SUFFIXES: .tex .bib .pdf

ifeq ($(shell uname),Darwin)
    PDFVIEWER=open
else
    PDFVIEWER=evince
endif

TARGET=draft-proposal

# IMAGES= planning-overdue-marks.eps planning-overdue.eps

.tex.pdf:
	pdflatex $* && pdflatex $*
.tex.dvi:
	latex $* && latex $*
.dvi.ps:
	dvips $*
.ps.pdf:
	ps2pdf $*.ps
.pdf.eps:
	pdf2ps $*.pdf $*.eps

all: $(TARGET).pdf view

$(TARGET).dvi: $(IMAGES)

view:
	$(PDFVIEWER) $(TARGET).pdf

clean:
	rm -f *.toc *.bm *.aux *.log *.nav *.out *.snm *.eps
	rm -f $(TARGET).dvi $(TARGET).ps $(TARGET).pdf
